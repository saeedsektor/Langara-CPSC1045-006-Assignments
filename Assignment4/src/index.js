//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");

function update(){
    let moveX, moveY, rotateObj, collection, translateString, rotateString;
    moveX = document.querySelector("#moveX");
    moveY = document.querySelector("#moveY");
    rotateObj = document.querySelector("#rotateObj");
    collection = document.querySelector("#collection");

    translateString = "translate( "+ Number(moveX.value) + " " + Number(moveY.value) +" )";
    collection.setAttribute("transform", translateString);

    rotateString = "rotate( " +rotateObj.value + " "+150 +" "+ 150+" )";

    let transformString = translateString + " "+ rotateString;
    collection.setAttribute("transform", transformString);

    console.log(translateString);
    console.log(rotateString);

}

window.update = update;