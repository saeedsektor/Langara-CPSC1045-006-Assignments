//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.

function changeRadii() {
    let inputR = document.querySelector("#inputR");
    Number(inputR.value);
    let v1 = inputR.value;
    let v2 = 200-inputR.value;
    circleLeft.setAttribute("r", v1);
    circleRight.setAttribute("r", v2);
    console.log(circleLeft);
    console.log(circleRight);
}

function changeSmile() {
  let smile1 = document.querySelector("#mySmile1");
  let smile2 = document.querySelector("#mySmile2");
  let smileControl = document.querySelector("#smileControl");
  let val = Number(smileControl.value);
  smile1.setAttribute("y1", val);
  smile2.setAttribute("y1", val);
}

function hairLength() {
  let myHair = document.querySelector("#myHair");
  let hairL = document.querySelector("#hairL");
  hairL = Number(hairL.value);
  let hairVal = "215,190 280,38 540,38 585,190";
  if (hairL == 0) {
    hairVal = "215,190 280,38 540,38 585,190";
  }else if (hairL == 1) {
    hairVal = "215,190 280,36 540,36 585,190";
  }else if (hairL == 2) {
    hairVal = "215,190 280,34 540,34 585,190";
  }else if (hairL == 3) {
    hairVal = "215,190 280,32 540,32 585,190";
  }else if (hairL == 4) {
    hairVal = "215,190 280,30 540,30 585,190";
  }else if (hairL == 5) {
    hairVal = "215,190 280,28 540,28 585,190";
  }
  myHair.setAttribute("points", hairVal);

}

function pupilsX() {
  let leftEye = document.querySelector("#leftEye");
  let rightEye = document.querySelector("#rightEye");
  let pupilsX = document.querySelector("#pupilsX");
  pupilsX = Number(pupilsX.value);
  leftEye.setAttribute("cx", 300+pupilsX);
  rightEye.setAttribute("cx", 470+pupilsX);

}

function pupilsY() {
  let pupilsY = document.querySelector("#pupilsY");
  pupilsY = Number(pupilsY.value);
  leftEye.setAttribute("cy", 240+pupilsY);
  rightEye.setAttribute("cy", 240+pupilsY);

}

function hairColor() {
  let hairColor = document.querySelector("#hairColor");
  myHair.setAttribute("fill", hairColor.value);
}

function nameControl() {
  let nameControl = document.querySelector("#nameControl");
  let myName = document.querySelector("#myName");
  myName.innerHTML = nameControl.value;
}



window.changeSmile = changeSmile;
window.hairLength = hairLength;
window.pupilsX = pupilsX;
window.pupilsY = pupilsY;
window.hairColor = hairColor;
window.nameControl = nameControl;
window.changeRadii = changeRadii;
